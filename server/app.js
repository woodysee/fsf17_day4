console.log("Started app.js - Server-side with Express middleware");

/*
* Server with Express middleware
*/

//Dependencies
var express = require('express'); //take in express middleware to create a web app server
var app = express(); //creates an Express object with the name "app"

console.log(`You are now at: ${__dirname}`); //print out the directory where I am on
const NODE_PORT = process.env.PORT || 4000; //take a port from my env variables (e.g. export PORT=4000 for Mac; set PORT=4000 for Windows; run `env` in Terminal to check PORT env variable). It will take the environment before the fallback of 4000.
console.log(`The port on this machine using this code is: ${NODE_PORT}`); //prints "4000"

app.use(express.static(__dirname + '/../client/')); //tell Express where all my client files are to serve

//Data
var popQuiz = require("./quizzes.json"); //imports popQuiz database

//Methods
app.get(
    "/api/popquizzes",
    (req, res) => {
        console.log("200: GET /api/popquizzes");
        res.status(200).json(popQuiz);
    }
);

app.post(
    "/api/submitquizzes",
    (req, res) => {
        console.log("200: POST /api/submitquizzes")
        console.log(req.body);
        res.status(200).json(popQuiz);
    }
)

// app.use(
//     (req, res) => {
//         var errTest = 5;
//         try {
//             if (errTest !== 5) {
//                 throw new Error("Not equals to 5");
//             };
//             console.log("404: Nothing was found!");
//             res.send("<h1>404: Nothing was found!</h1>");
//         } catch (exception) {
//             console.log(`exception == ${exception}`);
//             res.status(500).send(`500: Something wrong with express`);
//         }
//     }
// );

app.listen(
    NODE_PORT,
    () => {
        console.log(`Listening with server/app.js at port ${NODE_PORT}`);
        //console.log('My first node.js web app at' + NODE_PORT);
    }
);