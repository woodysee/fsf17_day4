# README #

My first Angular app.

### What is this repository for? ###

This allows us to set up an Angular 1.0 frontend web app with `node.js` web app as backend in the same repository.

### Setting up ###

* Set up the node.js back-end web app:
    - `git init`
    - `git remote add origin <git url>`
    - `npm init`
    (Answer all the questions , enter the entry point accordingly)
    - `mkdir <server directory>`
    - Create an `/app.js` under the server directory
    - Create `.gitignore` to ignore directories/files

* Create a `.bowerrc` with the contents:

        {
            "directory": "client/bower_components"
        }

* `bower init`

    - Set currently installed components as dependencies? **Yes**
    - Add commonly ignored files to ignore list? **Yes**
    - Would you like to mark this package as private which prevents it from being accidentally published to the registry? **No**

* `bower install bootstrap font-awesome angular --save`

## Tips ##
* <code>bower_components</code> must be inside <code>client/</code> while <code>.bowerrc</code> must be at the project level for single projects.