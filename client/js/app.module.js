/*
**  Client (Module) with AngularJS
*/
"use strict";
console.log("Started app.module.js - Client with AngularJS");

( 
    () => {
        angular.module("PopQuizApp", []);
        //The square brackets is related to `DI` which is called Dependency Injection, invented by ThoughtWorks founder. Which allows easy interaction and injection
    }
)();

