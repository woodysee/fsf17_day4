/*
**  Client Controller (ViewModel) with AngularJS
*/
"use strict";
console.log("Started app.controller.js - Client with AngularJS");

(
    () => {
        angular.module("PopQuizApp").controller("PopQuizController",PopQuizController);
        
        PopQuizController.$inject = ["$http"]; //initialisation of communication to our Express.js backend using a HTTP request
        function PopQuizController($http) {
            const self = this;
            self.quiz = {}; //init quiz as empty object
            self.finalAnswer = {
                choice: "",
                remarks: ""
            };

            //Get backend data
            self.initForm = () => {
                $http.get("/api/popquizzes").then(
                    (result) => {
                        console.log(result);
                        self.quiz = result.data;
                    }
                ).catch(
                    (e) => {
                        console.log(e);
                    }
                );
            };
            self.initForm();

            self.submitQuiz = () => {
                console.log(self.finalAnswer.choice);
                console.log(self.finalAnswer.remarks);
                $http.post(
                    "/api/submitquizzes",
                    self.finalAnswer
                ).then(
                    (result) => {
                        self.isCorrect = result.data;
                        console.log(result.data);
                    }
                ).catch(
                    (e) => {
                        console.log(e);
                    }
                );
            };
            self.submitQuiz();
        };
    }
)();